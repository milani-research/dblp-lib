module Web.DBLP.Utils where

import qualified Data.Text as T
import qualified Data.Text.Encoding      as T
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.Lazy.Encoding as TL
import Network.HTTP.Client
import Network.HTTP.Types.Status
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except

fetchUrl :: String -> ExceptT String IO TL.Text
fetchUrl url = do
  response <- lift $ do
    manager <- newManager defaultManagerSettings
    request <- parseRequest url
    httpLbs request manager
  if (responseStatus response) == ok200 then
    either (throwE . show)
           (return)
           (TL.decodeUtf8' $ responseBody response)
  else
    throwE $ either show T.unpack $ T.decodeUtf8' $ statusMessage $ responseStatus response

