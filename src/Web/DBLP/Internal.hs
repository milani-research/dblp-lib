module Web.DBLP.Internal where

import Web.DBLP.Entities
import Web.DBLP.Utils
import qualified Data.FuzzySet as Fuzzy

publicationQuery query = "https://dblp.org/search/publ/api?q="
                         ++ (escapeURI $ intercalate "+" $ words $ map toLower query)
                         ++ "&format=json&h=30&f=1&c=0"

authorQuery query = "https://dblp.org/search/author/api?q="
                  ++ (escapeURI $ intercalate "+" $ words $ map toLower query)
                  ++ "&format=json&h=30&f=1&c=0"

venueQuery query = "https://dblp.org/search/venue/api?q="
                    ++ (escapeURI $ intercalate "+" $ words $ map toLower query)
                    ++ "&format=json&h=30&f=1&c=0"

searchAuthor :: String -> ExceptT String IO ([Author])
searchAuthor name = do
  let url = authorQuery name
  body <- fetchUrl url
  return $ parseAuthors body

findAuthor :: String -> ExceptT String IO (Maybe Author)
findAuthor name = do
  candidates <- searchAuthor name
  return $ findBestMatchBy name authorName candidates

searchPublication :: String -> ExceptT String IO ([Publication])
searchPublication name = do
  let url = publicationQuery name
  body <- fetchUrl
  return $ parsePublications body

findPublication :: String -> ExceptT String IO (Maybe Publication)
findPublication name = do
  candidates <- searchPublication name
  return $ findBestMatchBy name publicationTitle candidates

searchVenue :: String -> ExceptT String IO ([Venue])
searchVenue name = do
  let url = venueQuery name
  body <- fetchUrl
  return $ parseVenues body

findVenue :: String -> ExceptT String IO (Maybe Venue)
findVenue name = do
  candidates <- searchVenue name
  return $ findBestMatchBy name venueName candidates

findBestMatchBy = findBestMatchBy' 2 4

findBestMatchBy' k0 k1 query f xs =
  let table = foldr (\x m -> M.insert (f x) x m) M.empty xs
  in Fuzzy.runFuzzySearch
      ( do
        Fuzzy.addMany $ map f xs
        v <- Fuzzy.closestMatch query
        return $ fmap (M.!) v
      )
     k0
     k1
     False
  
