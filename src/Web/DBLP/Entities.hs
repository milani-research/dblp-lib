{-# LANGUAGE OverloadedStrings #-}
module Web.DBLP.Entities
where

import Data.Time.Calendar (Day)
import qualified Data.Text               as T
import qualified Data.Text.Encoding      as T
import qualified Data.YAML as YAML
import Data.Maybe

type DOI = String

data Publication = Publication
  { publicationTitle   :: Maybe String
  , publicationDate    :: Maybe Day
  , publicationAuthors :: Maybe [String]
  , publicationDOI     :: Maybe DOI
  , publicationURL     :: Maybe String
  , publicationVenue   :: Maybe String
  , publicationBibTeX  :: Maybe String
  }

data Author = Author
  { authorName :: String
  , authorPublications :: [Publication]
  }

-- instance YAML.FromYAML Day where
--  parseYAML = 

instance YAML.FromYAML Publication where
  parseYAML = YAML.withMap "Publication" $ \m -> Publication
      <$> (fmap (fmap T.unpack) $ m YAML..:! "title")
      <*> (fmap (fmap (read . T.unpack)) $ m YAML..:! "date") -- TODO: Use mread instead
      <*> (fmap (fmap (map T.unpack)) $ m YAML..:! "authors")
      <*> (fmap (fmap T.unpack) $ m YAML..:! "doi")
      <*> (fmap (fmap T.unpack) $ m YAML..:! "url")
      <*> (fmap (fmap T.unpack) $ m YAML..:! "venue")
      <*> (fmap (fmap T.unpack) $ m YAML..:! "bibtex")

instance YAML.ToYAML Publication where
  toYAML publication = YAML.mapping $
    (maybe [] (\title  -> ["title" YAML..= (T.pack title)]) $ publicationTitle publication)
    ++ (maybe [] (\date      -> ["date" YAML..= (T.pack $ show date)]) $ publicationDate publication)
    ++ (maybe [] (\authors  -> ["authors" YAML..= (map T.pack authors)]) $ publicationAuthors publication)
    ++ (maybe [] (\venue  -> ["venue" YAML..= (T.pack venue)]) $ publicationVenue publication)
    ++ (maybe [] (\doi    -> ["doi" YAML..= (T.pack doi)]) $ publicationDOI publication)
    ++ (maybe [] (\url    -> ["url" YAML..= (T.pack url)]) $ publicationURL publication)
    ++ (maybe [] (\bibtex -> ["bibtex" YAML..= (T.pack bibtex)]) $ publicationBibTeX publication)

instance YAML.FromYAML Author where
  parseYAML = YAML.withMap "Author" $ \m -> Author
      <$> (fmap (maybe "" T.unpack) $ m YAML..:! "name")
      <*> (fmap (fromMaybe []) $ m YAML..:! "publications")

instance YAML.ToYAML Author where
  toYAML author = YAML.mapping $
    [ "name" YAML..= (T.pack $ authorName author)
    , "publications" YAML..= (map YAML.toYAML $ authorPublications author)
    ]
