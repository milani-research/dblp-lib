module Web.DBLP
       ( searchAuthor
       , searchPublication
       , searchVenue
       , coauthors
       )
where

import Web.DBLP.Internal
import Web.DBLP.Entities
