{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, aeson, base, bytestring, containers
      , directory, filepath, fuzzyset, HsYAML, http-client, http-types
      , HUnit, lib, network-uri, text, time, transformers
      }:
      mkDerivation {
        pname = "dblp";
        version = "0.1.0.0";
        src = ./.;
        libraryHaskellDepends = [
          aeson base bytestring containers directory filepath fuzzyset HsYAML
          http-client http-types network-uri text time transformers
        ];
        testHaskellDepends = [ base HUnit ];
        description = "DBLP API for searching scientific articles, reserachers, conferences, etc";
        license = lib.licenses.gpl3Only;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
